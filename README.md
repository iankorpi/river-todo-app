# River To-do App

A minimal to-do app based on Mark Forster's [autofocus](http://markforster.squarespace.com/autofocus-system/) time management system.

## Dependencies

This app was written with zero dependencies, using only built-in ECMAScript 6 features.