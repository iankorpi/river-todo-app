import { TodoList } from './todo.js';

function handleAddEntry() {
    var element = document.getElementById("todoInput");
    var content = element.value;
    if (content != '') { // don't add empty tasks
        todo.addItem(content);
        element.value = '';
    }
}

const todo = new TodoList('todo-container');

document.getElementById('addEntryButton').addEventListener('click', handleAddEntry);

document.addEventListener("keypress", function(e) {
    if (e.key === "Enter") {
        handleAddEntry();
    }
})