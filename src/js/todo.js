class TodoList {
    constructor (targetId) {
        this.__nextId = 0;
        this.baseElement = document.getElementById(targetId);
        this.tasks = [];

        this.listElement = document.createElement('ul');
        this.listElement.classList.add('todo');
        this.baseElement.appendChild(this.listElement);
    }

    addItem(label, id=null) { /* adds a task */
        if (!id) {
            id = this.getNextId();
        }
        var task = new Task(label, id, this);
        this.tasks.push(task);
        this.listElement.appendChild(task);
    };

    removeItem() { /* removes a task. */

    };

    getNextId() { // get next id number and increment.
        var id = this.__nextId;
        this.__nextId++;

        return id;
    }
}

class Task {
    constructor (description, id, parent) {
        // binding methods
        this.markComplete = this.markComplete.bind(this);
        this.markProgress = this.markProgress.bind(this);

        this.parent = parent;
        this.id = id;
        this.description = description;

        let element = document.createElement('li');
        element.setAttribute("id", id);
    
        let label = document.createElement('span');
        label.classList.add('task-description');
        label.textContent = description;
    
        // build complete button
        let completeButton = document.createElement('button');
        completeButton.classList.add('complete-task');
        completeButton.textContent = '✓';
        completeButton.ariaLabel = 'Mark complete';
        completeButton.onclick = this.markComplete;

        // build progress button
        let progressButton = document.createElement('button')
        progressButton.classList.add('progress-task');
        progressButton.textContent = '🡣';
        progressButton.ariaLabel = 'Mark progress';
        progressButton.onclick = this.markProgress;

        element.appendChild(completeButton);
        element.appendChild(label);
        element.appendChild(progressButton);
        

        return element;
    }

    markComplete() { /* crosses task off the list. */
        let element = document.getElementById(this.id);
        
        let completeButton = element.getElementsByClassName('complete-task').item(0);
        let progressButton = element.getElementsByClassName('progress-task').item(0);
        let label = element.getElementsByClassName('task-description').item(0);

        completeButton.setAttribute('style', 'display: none;');
        progressButton.setAttribute('style', 'display: none;');
        label.classList.add('complete');
    };

    markProgress() { /* for a given task, crosses it off the list and appends it to the end. */

        console.log(this);
        this.parent.addItem(this.description);
        this.markComplete();
    };
}

export { Task, TodoList };